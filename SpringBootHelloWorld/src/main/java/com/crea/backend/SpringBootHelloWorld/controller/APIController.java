package com.crea.backend.SpringBootHelloWorld.controller;


import com.crea.backend.SpringBootHelloWorld.json.ratesApi.RatesResponse;
import com.crea.backend.SpringBootHelloWorld.service.RestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Api(description = "External APIs management")
@RestController
@RequestMapping(path = "/api")
public class APIController {

    /**
     * Gets currency rates as plain json from api
     *
     * @return The api response as plain json
     */
    @CrossOrigin
    @ApiOperation(value = "Get the exchange rates of many currencies as plain JSON")
    @GetMapping(value = "/rates")
    public String GetCurrencyRates() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestService restService = new RestService(restTemplateBuilder);
        return restService.getResponseAsPlainJSON("https://api.exchangeratesapi.io/latest?base=USD");
    }

    /**
     * Gets currency rates as RatesResponse object from api
     *
     * @return The api response as a RatesResponse Object
     */
    @CrossOrigin
    @ApiOperation(value = "Get the exchange rates of many currencies as Object")
    @GetMapping(value = "/ratesAsObject")
    public RatesResponse GetCurrencyRatesAsObject() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestService restService = new RestService(restTemplateBuilder);
        return restService.getRatesAsObject();
    }


    /**
     * Gets a random chuck norris joke
     *
     * @return The api response as plain json
     */
    @CrossOrigin
    @ApiOperation(value = "Get a random chuck norris joke")
    @GetMapping(value = "/chucknorris")
    public String GetChuckNorrisJoke() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestService restService = new RestService(restTemplateBuilder);
        return restService.getResponseAsPlainJSON("https://api.chucknorris.io/jokes/random");
    }

}
