package com.crea.backend.SpringBootHelloWorld.controller;

import com.crea.backend.SpringBootHelloWorld.exception.ProductNotFoundException;
import com.crea.backend.SpringBootHelloWorld.model.Product;

import com.crea.backend.SpringBootHelloWorld.dao.ProductDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Api(description = "Product management")
@RestController
//@RequestMapping(path = "/produit/")
public class ProductController {

    @Autowired
    private ProductDao productDao;


    // Get request sends id to path
    @ApiOperation(value = "Display a specific product")
    @GetMapping(value = "/produit/{id}")
    public Product DisplayProduct(@PathVariable int id) {
        Product productFound = productDao.findById(id);
        if (productFound == null) throw new ProductNotFoundException("No product with id : " + id);
        return productFound;
    }

    @ApiOperation(value = "Retrieve the product with a greater price than the {prix} parameter")
    @GetMapping(value = "/produit/findProductByPrixGreaterThan/{prix}")
    public List<Product> FindProductByPriceGreaterThan(@PathVariable int prix) {
        return productDao.findProductByPrixGreaterThan(prix);
    }


    //Récupérer la liste des produits
    @ApiOperation(value = "Retrieve the list of all products")
    @RequestMapping(value = "/produits", method = RequestMethod.GET)
    public List<Product> listeProduits() {
        return productDao.findAll();
    }

    // Add product
    @ApiOperation(value = "Add a new product")
    @PostMapping(value = "/produits")
    public void ajouterProduit(@RequestBody Product product) {
        productDao.save(product);
    }

    // Delete product
    @ApiOperation(value = "Delete product")
    @DeleteMapping(value = "/produit/{id}")
    public void DeleteProduct(@PathVariable int id) {
        productDao.delete(productDao.findById(id));
    }
}
