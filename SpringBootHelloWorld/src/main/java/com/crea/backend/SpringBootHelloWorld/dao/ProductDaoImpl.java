/*package com.crea.backend.SpringBootHelloWorld.dao;

import com.crea.backend.SpringBootHelloWorld.model.Product;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

// Indique que c'est comme une database
@Repository
public class ProductDaoImpl implements ProductDao {

    public static List<Product> products = new ArrayList<>();

    static {
        products.add(new Product(1, "OnePlus", "Phone", 350));
        products.add(new Product(2, "IPhone", "Phone", 350));
        products.add(new Product(3, "Samsung", "Phone", 350));
    }

    @Override
    public List<Product> findAll() {
        return products;
    }

    @Override
    public Product findById(int id) {
        //return products.get(id);
        for (Product product : products) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    @Override
    public Product save(Product p) {
        products.add(p);
        return p;
    }

    @Override
    public boolean delete(int id) {
        for (Product product : products) {
            if (product.getId() == id) {
                if (products.remove(product)) {
                    return true;
                }
            }
        }
        return false;
    }
}
*/