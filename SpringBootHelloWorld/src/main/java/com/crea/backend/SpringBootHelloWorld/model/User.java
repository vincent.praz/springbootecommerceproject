package com.crea.backend.SpringBootHelloWorld.model;


import javax.persistence.*;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //@Length(min=3,max=20,message="Trop long ou trop court!")
    private String name;
    private String password;

    // User can have favorite products
    @ManyToMany
    private List<Product> favouriteProducts;

    /**
     * User Constructor
     *
     * @param id                user id
     * @param name              user name
     * @param password          user password
     * @param favouriteProducts favorite products list
     */
    public User(int id, String name, String password, List<Product> favouriteProducts) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.favouriteProducts = favouriteProducts;
    }

    /**
     * User Constructor without productlist
     *
     * @param id       user id
     * @param name     user name
     * @param password user password
     */
    public User(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public User() {

    }

    public int getId() {
        return id;
    }

    public List<Product> getFavouriteProducts() {
        return favouriteProducts;
    }

    public void setFavouriteProducts(List<Product> favouriteProducts) {
        this.favouriteProducts = favouriteProducts;
    }

    /**
     * Adds a product to the favourite products list
     *
     * @param product to be added
     */
    public void addFavouriteProduct(Product product) {
        if (!this.favouriteProducts.contains(product)) {
            this.favouriteProducts.add(product);
        }
    }

    /**
     * Removes a product from the favourite products list
     *
     * @param product to be removed
     */
    public void removeFavouriteProduct(Product product) {
        this.favouriteProducts.remove(product);
    }


    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
