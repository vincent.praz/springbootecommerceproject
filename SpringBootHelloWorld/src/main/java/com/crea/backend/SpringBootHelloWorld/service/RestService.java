package com.crea.backend.SpringBootHelloWorld.service;

import com.crea.backend.SpringBootHelloWorld.json.ratesApi.RatesResponse;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    /**
     * Gets a generic Response as plain JSON
     *
     * @param url of the api
     * @return the api response (plain json)
     */
    public String getResponseAsPlainJSON(String url) {
        return this.restTemplate.getForObject(url, String.class);
    }


    /**
     * Gets the rates as a RatesResponse object
     *
     * @return the RatesResponse Object
     */
    public RatesResponse getRatesAsObject() {
        String url = "https://api.exchangeratesapi.io/latest?base=USD";
        return this.restTemplate.getForObject(url, RatesResponse.class);
    }
}