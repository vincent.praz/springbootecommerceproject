package com.crea.backend.SpringBootHelloWorld.dao;

import com.crea.backend.SpringBootHelloWorld.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
    List<User> findAll();

    User findById(int id);

    User findByNameAndPassword(String name, String password);

    User save(User p);
}
