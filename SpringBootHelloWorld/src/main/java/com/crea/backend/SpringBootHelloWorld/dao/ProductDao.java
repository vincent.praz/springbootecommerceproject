package com.crea.backend.SpringBootHelloWorld.dao;

import com.crea.backend.SpringBootHelloWorld.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {

    List<Product> findAll();

    Product findById(int id);

    Product save(Product p);

    List<Product> findProductByPrixGreaterThan(int prix);
}
