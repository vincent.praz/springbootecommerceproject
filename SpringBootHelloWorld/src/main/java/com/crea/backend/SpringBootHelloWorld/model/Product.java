package com.crea.backend.SpringBootHelloWorld.model;


import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Length(min=3,max=20,message="Trop long ou trop court!")
    private String nom;
    private String category;
    private int prix;

    /**
     * Product Constructor
     *
     * @param id product id
     * @param nom product name
     * @param category product category
     * @param prix product price
     */
    public Product(int id, String nom, String category, int prix) {
        this.id = id;
        this.nom = nom;
        this.category = category;
        this.prix = prix;
    }


    public Product() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }
}
