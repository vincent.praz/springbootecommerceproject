- SpringBootHelloWorld : Backend (url : https://localhost:8443)
- springbootfrontendecommerce : Frontend (url : http://localhost:3000)

Database : H2 => See DatabaseSchema.png

Swagger is enabled (config : configuration/SwaggerConfig.java)
WebSecurity is enabled (config : configuration/WebSecurityConfig.java)
GraphQL is enabled on queries findById (Product) and findUserById (url : https://localhost:8443/graphiql)

You should have access too all frontend pages without any CORS nor CSRF errors