import React from "react";
import styled from "styled-components";

const InputStyled = styled.input`
    background-color: #3D005C;
    color : white;
    border: 1px solid grey;
    margin-right: 10px;
    margin-bottom: 10px;
    padding: 5px;
`;

const Input = ({value, setValue, label, error, type}) => (
    <InputStyled type={type} value={value} onChange={(e) => setValue(e.target.value)} placeholder={label}
           style={{borderColor: error ? "red" : "black"}}/>
);

export default Input;
