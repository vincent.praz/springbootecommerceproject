import React, {useState} from "react";
import Input from "../components/Input";
import {Link} from "react-router-dom";
import axios from "axios";
import styled from "styled-components";

const AddToFavourite = () => {

    const [userId, setUserId] = useState("");
    const [users, setUsers] = useState(null);
    const [products, setProducts] = useState(null);
    const [productId, setProductId] = useState("");
    const [error, setError] = useState(null);

    const StyledButtons = styled.button`
        color: white;
        background-color: #3d005c;
        box-shadow: 0px 0px 0px transparent;
        border: 0px solid transparent;
        text-shadow: 0px 0px 0px transparent;
        border-radius:25px;
        padding : 6px;
    `;


    /**
     * Retrieves User list
     */
    const getUserList = () => {
        axios
            .get("https://localhost:8443/users")
            .then(function (response) {
                setUsers(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    /**
     * Retrieves Product list
     */
    const getProductList = () => {
        axios
            .get("https://localhost:8443/produits")
            .then(function (response) {
                setProducts(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    };

    const addToFavourite = () => {
        axios
            .post("https://localhost:8443/user/" + userId + "/addProductToFav/" + productId, {})
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });
    };


    const removeFromFavourite = () => {
        axios
            .post("https://localhost:8443/user/" + userId + "/removeProductToFav/" + productId, {})
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });
    };


    /**
     * Validate form and retrieve calls the addToFavourite function
     */
    const onSubmit = () => {
        setError(null);
        if (!userId || userId === "") {
            setError({field: "userId", message: "Please enter your userId"});
        } else if (!productId || productId === "") {
            setError({field: "productId", message: "Please enter your productId"});
        } else {
            // Adds to favourite
            setUserId(userId);
            setProductId(productId);
            addToFavourite();
            getUserList();
            console.log("User info properly entered");
        }
    }


    /**
     * Validate form and retrieve calls the addToFavourite function
     */
    const onSubmitRemove = () => {
        setError(null);
        if (!userId || userId === "") {
            setError({field: "userId", message: "Please enter your userId"});
        } else if (!productId || productId === "") {
            setError({field: "productId", message: "Please enter your productId"});
        } else {
            // Adds to favourite
            setUserId(userId);
            setProductId(productId);
            removeFromFavourite();
            getUserList();
            console.log("User info properly entered");
        }
    }

    const onSubmitUList = () => {
        getUserList();
    }

    const onSubmitPList = () => {
        getProductList();
    }


    return (
        <div className="CreateUser">
            <Link to="/">Home</Link>

            <h1>Users</h1>
            {users != null &&
            JSON.stringify(users)
            }

            <StyledButtons onClick={onSubmitUList}>Refresh</StyledButtons>

            <h1>Products</h1>
            {products != null &&
            JSON.stringify(products)
            }

            <StyledButtons onClick={onSubmitPList}>Refresh</StyledButtons>

            <h1>Add Favourite products</h1>
            <Input
                type={"text"}
                value={userId}
                setValue={setUserId}
                label="userId"
                error={error?.field === "userId"}
            />
            <Input
                type={"text"}
                value={productId}
                setValue={setProductId}
                label="productId"
                error={error?.field === "productId"}
            />
            {error && <div>Error: {error.message}</div>}
            <br/>
            <StyledButtons onClick={onSubmit}>Add To Favourite</StyledButtons>



            <h1>Remove Favourite products</h1>
            <Input
                type={"text"}
                value={userId}
                setValue={setUserId}
                label="userId"
                error={error?.field === "userId"}
            />
            <Input
                type={"text"}
                value={productId}
                setValue={setProductId}
                label="productId"
                error={error?.field === "productId"}
            />
            {error && <div>Error: {error.message}</div>}
            <br/>
            <StyledButtons onClick={onSubmitRemove}>Remove from Favourite</StyledButtons>
        </div>
    );
}

export default AddToFavourite;
