import React, {useState} from "react";
import Input from "../components/Input";
import {Link, useHistory} from "react-router-dom";
import axios from "axios";
import styled from "styled-components";

const CreateUser = () => {

    const history = useHistory();

    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);

    const StyledButtons = styled.button`
        color: white;
        background-color: #3d005c;
        box-shadow: 0px 0px 0px transparent;
        border: 0px solid transparent;
        text-shadow: 0px 0px 0px transparent;
        border-radius:25px;
        padding : 6px;
    `;

    const createUser = (name, password) => {
        axios
            .post("https://localhost:8443/users", {
                name: name,
                password: password,
            })
            .then(function (response) {

            })
            .catch(function (error) {
                console.log(error);
            });
    };


    /**
     * Validate form and retrieve calls the createUser function
     */
    const onSubmit = () => {
        setError(null);
        if (!name || name === "") {
            setError({field: "email", message: "Please enter your name"});
        } else if (!password || password === "") {
            setError({field: "password", message: "Please enter your password"});
        }else {
            // Creates the user
            createUser(name, password);
            console.log("User info properly entered");

            // auto-Redirect to login page
            history.push("/Login");
        }
    }

    return (
        <div className="CreateUser">
            <Link to="/">Home</Link>
            <h1>Enter your informations</h1>
            <Input
                type={"text"}
                value={name}
                setValue={setName}
                label="name"
                error={error?.field === "name"}
            />
            <Input
                type={"password"}
                value={password}
                setValue={setPassword}
                label="*********"
                error={error?.field === "password"}
            />
            {error && <div>Error: {error.message}</div>}
            <br/>
            <StyledButtons onClick={onSubmit}>Submit</StyledButtons>
            <Link to="/Login">Back to login page</Link>
        </div>
    );
}

export default CreateUser;
