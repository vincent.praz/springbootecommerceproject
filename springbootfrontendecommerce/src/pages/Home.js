import React from "react";
import {Link} from "react-router-dom";

const Home = () => {
    return (
        <div>
            <h1>With these links you will be able to test all the Spring ecommerce application</h1>
            <ul>
                <li><Link to="/Login">Inscription et connexion</Link></li>
                <li><Link to="/AddToFavourite">Ajout de produits en favoris</Link></li>
                <li><Link to="/ExternalAPI">Appel des api externes</Link></li>
            </ul>
        </div>

    );
}

export default Home;
