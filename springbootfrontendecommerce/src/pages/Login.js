import React, {useState} from "react";
import Input from "../components/Input";
import {Link} from "react-router-dom";
import axios from "axios";
import styled from "styled-components";

const Login = () => {
    const [connected, setConnected] = useState(false);
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(null);


    const StyledButtons = styled.button`
        color: white;
        background-color: #3d005c;
        box-shadow: 0px 0px 0px transparent;
        border: 0px solid transparent;
        text-shadow: 0px 0px 0px transparent;
        border-radius:25px;
        padding : 6px;
    `;


    const login = (name, password) => {
        axios
            .post("https://localhost:8443/user/login", {
                name: name,
                password: password,
            })
            .then(function (response) {
                if (response.data === true) {
                    setConnected(true);
                }else{
                    setConnected(false);
                }
            })
            .catch(function (error) {
                console.log(error);
            });
    };


    /**
     * Validate form and retrieve calls the login function
     */
    const onSubmit = () => {
        setError(null);
        if (!name || setName === "") {
            setError({field: "email", message: "Please enter your email"});
        } else if (!password || password === "") {
            setError({field: "password", message: "Please enter your password"});
        } else {

            // Check login
            login(name, password);

            console.log("User info properly entered");
        }
    }

    return (
        <div>
            <Link to="/">Home</Link>
            <h1>Login</h1>
            <p>
                The user is <b>{connected ? 'currently' : 'not'}</b> logged in.
            </p>
            <Input
                type={"text"}
                value={name}
                setValue={setName}
                label="name"
                error={error?.field === "name"}
            />
            <Input
                type={"password"}
                value={password}
                setValue={setPassword}
                label="*********"
                error={error?.field === "password"}
            />
            {error && <div>Error: {error.message}</div>}
            <StyledButtons onClick={onSubmit}>Submit</StyledButtons>
            <Link to="/create">Sign Up</Link>
        </div>
    );
}

export default Login;
