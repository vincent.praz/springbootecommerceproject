import React, {useState} from "react";
import {Link} from "react-router-dom";
import axios from "axios";
import styled from "styled-components";

const ExternalAPI = () => {
    const [chuck, setChuck] = useState("");
    const [rate, setRate] = useState(null);


    const StyledButtons = styled.button`
        color: white;
        background-color: #3d005c;
        box-shadow: 0px 0px 0px transparent;
        border: 0px solid transparent;
        text-shadow: 0px 0px 0px transparent;
        border-radius:25px;
        padding : 6px;
    `;


    const chuckAPI = () => {
        axios
            .get("https://localhost:8443/api/chucknorris")
            .then(function (response) {
                setChuck(response.data)
            })
            .catch(function (error) {
                console.log(error);
            });
    };



    const ratesAPI = () => {
        axios
            .get("https://localhost:8443/api/ratesAsObject")
            .then(function (response) {
                setRate(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    };


    return (
        <div>
            <Link to="/">Home</Link>
            <h1>APIS</h1>

            <h2>Generate a chuck norris joke (API Laggs a bit)</h2>
            {chuck}
            <br/>
            <StyledButtons onClick={chuckAPI}>Generate</StyledButtons>

            <h2>Check the actual currency exchange rates</h2>
            {rate != null &&
                <div>
                    {JSON.stringify(rate)}
                </div>
            }
            <br/>
            <StyledButtons onClick={ratesAPI()}>Check</StyledButtons>
        </div>
    );
}

export default ExternalAPI;
