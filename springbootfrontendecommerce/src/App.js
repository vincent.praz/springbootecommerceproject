import React from 'react';
import './App.css';

import {BrowserRouter as Router, Route, Switch} from "react-router-dom";

import Home from "./pages/Home"
import Login from "./pages/Login"
import CreateUser from "./pages/CreateUser";
import ExternalAPI from "./pages/ExternalAPI";
import AddToFavourite from "./pages/AddToFavourite";

function App() {
    return (
        <Router>
            <Switch>
                <>
                    <Route path="/" exact component={Home}/>
                    <Route path="/login" exact component={Login}/>
                    <Route path="/create" exact component={CreateUser}/>
                    <Route path="/ExternalAPI" exact component={ExternalAPI}/>
                    <Route path="/AddToFavourite" exact component={AddToFavourite}/>
                </>
            </Switch>
        </Router>
    );
}

export default App;
